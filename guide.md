# Obtaining a desk

The lab is spread across five rooms: MC 106, MC107, MC108, MC111 and MC112.

In order to obtain a desk, check first with the people in the lab for free spots.

To unlock MC111, you must first visit Ron Simpson on the second floor and ask for access with your McGill ID.

Currently, MC106, MC107, MC108 and MC112 have mechanical combination locks.

# Lab Meetings

Subscribe to the labrl mailing list at  http://mailman.cs.mcgill.ca/mailman/listinfo/labrl to get notified on upcoming meetings and announcements. 

The schedule is rather flexible and you are strongly encouraged to present or suggest presenters and topics of interest.

A lab member is usually responsible for buying Tim Horton's coffee and donuts (or cookies). Clement Gehring needs his '''triple''' chocolate cookie.
