{| class="wikitable"
|-
! scope="col"| English
! scope="col"| French
! scope="col"| Reference
|-
! scope="row"| Markov Decision Process
| Processus de décision de Markov
| [http://fr.wikipedia.org/wiki/Processus_de_d%C3%A9cision_markovien Wikipedia]
|-
! scope="row"| Labelled Markov Processes
| Processus de Markov étiquettés
| n/a
|-
! scope="row"| Factored MDP
| Processus de Décision Markovien Factorisés
| [http://people.bordeaux.inria.fr/degris/papers/These_Thomas_Degris.pdf Thomas Degris' PhD thesis ]
|-
! scope="row"| Reinforcement Learning
| Apprentissage par renforcement
| [http://people.bordeaux.inria.fr/degris/papers/These_Thomas_Degris.pdf Thomas Degris' PhD thesis ]
|-
! scope="row"| Value Function
| Fonction de valeur
| [http://people.bordeaux.inria.fr/degris/papers/These_Thomas_Degris.pdf Thomas Degris' PhD thesis ]
|-
! scope="row" | Reward Function
| Fonction de récompense
| [http://people.bordeaux.inria.fr/degris/papers/These_Thomas_Degris.pdf Thomas Degris' PhD thesis ]
|-
! scope="row" | Policy
| Politique
| [http://people.bordeaux.inria.fr/degris/papers/These_Thomas_Degris.pdf Thomas Degris' PhD thesis ]
|-
! scope="row"| Embedding
| Plongement
| [http://fr.wikipedia.org/wiki/Plongement Wikipedia]
|-
! scope="row"| Time Delay Embedding Method
| Plongement retardé dans le temps, méthode des coordonées retardées, méthode des retards
| [http://books.google.ca/books/about/Analyse_des_modes_chaotiques_dans_un_mot.html?id=I_hStwAACAAJ Analyse des modes chaotiques dans un moteur linéaire à réluctance variable en vue du contrôle ]
|-
! score="row" | Supervised Learning
| Apprentissage supervisé
| n/a
|-
! score="row" | Decision Tree
| Arbre de décision
| n/a
|-
! scope="row" | Feature extraction
| Extraction de caractéristiques
| [http://info.usherbrooke.ca/hlarochelle/publications/thesis.pdf Hugo Larochelle's PhD thesis ]
|-
! scope="row" | Likelihood
| Vraisemblance
| [http://info.usherbrooke.ca/hlarochelle/publications/thesis.pdf Hugo Larochelle's PhD thesis ]
|-
! scope="row" | Prior distribution
| Distribution a priori
| [http://info.usherbrooke.ca/hlarochelle/publications/thesis.pdf Hugo Larochelle's PhD thesis ]
|-
! scope="row" | Empirical risk minimization
| Minimisation du risque empirique
| [http://info.usherbrooke.ca/hlarochelle/publications/thesis.pdf Hugo Larochelle's PhD thesis ]
|-
! scope="row" | Kernel machine
| Machines à noyau
| [http://info.usherbrooke.ca/hlarochelle/publications/thesis.pdf Hugo Larochelle's PhD thesis ]
|-
! scope="row" | Manifold
| Variété
| [http://info.usherbrooke.ca/hlarochelle/publications/thesis.pdf Hugo Larochelle's PhD thesis ]
|-
! scope="row" | Clustering
| Groupage
| [http://info.usherbrooke.ca/hlarochelle/publications/thesis.pdf Hugo Larochelle's PhD thesis ]
|-
! scope="row" | Backpropagation
| Rétropropagation de gradient
| [http://info.usherbrooke.ca/hlarochelle/publications/thesis.pdf Hugo Larochelle's PhD thesis ]
|-
! scope="row" | Expectation
| Espérance
| [http://info.usherbrooke.ca/hlarochelle/publications/thesis.pdf Hugo Larochelle's PhD thesis ]
|-
! scope="row" | Sparse coding
| Représentation creuse
| [http://info.usherbrooke.ca/hlarochelle/publications/thesis.pdf Hugo Larochelle's PhD thesis ]
|-
! scope="row" | Bias-variance tradeoff
| Compromis biais-variance
| [http://info.usherbrooke.ca/hlarochelle/publications/thesis.pdf Hugo Larochelle's PhD thesis ]
|-
! scope="row" | Curse of dimensionality
| Fléau de la dimensionnalité
| [http://info.usherbrooke.ca/hlarochelle/publications/thesis.pdf Hugo Larochelle's PhD thesis ]
|-
|}
