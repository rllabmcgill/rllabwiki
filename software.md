# Reinforcement Learning
## Python
* The small and easy to hack [https://github.com/amarack/python-rl Python-RL] by [http://people.cs.umass.edu/~wdabney/ Will Dabney]
*  [http://acl.mit.edu/RLPy/ RLPy]  from the [http://acl.mit.edu/ ACL] lab at MIT.
```
 @online{RLPy,
    author = {Alborz Geramifard, Robert H Klein, Christoph Dann, William Dabney, Jonathan P How},
    title = {RLPy: The Reinforcement Learning Library for Education and Research},
    month = April,
    year = {2013},
    howpublished = {\url{http://acl.mit.edu/RLPy}},
}
```

* [http://pybrain.org/ PyBrain]

```
 @article{pybrain2010jmlr,
    author = {Schaul, Tom and Bayer, Justin and Wierstra, Daan and Sun, Yi and Felder, Martin and Sehnke, Frank and R{\"u}ckstie{\ss}, Thomas and Schmidhuber, J{\"u}rgen},
    journal = {Journal of Machine Learning Research},
    pages = {743--746},
    title = {PyBrain},
    volume = {11},
    year = {2010}
}
```

# Java
* [http://rlpark.github.io/ RLPark] You may want to talk to [http://www.cs.mcgill.ca/~cgehri/ Clement Gehring] for help.

# C++
* [https://github.com/samindaa/RLLib RLLib]
* Todd Hester's [http://wiki.ros.org/reinforcement_learning reinforcement_learning] package for ROS

# Machine Learning
* The powerful and easy to use [http://scikit-learn.org/stable/ scikit-learn] for Python. Hacking is feasible but might involve delving into some of its C bindings.
* [http://www.cs.ubc.ca/research/flann/ FLANN] for Approximate Nearest Neighbors in C++ with bindings in C, Matlab and Python
```
 @inproceedings{muja_flann_2009,
  author    = {Marius Muja and David G. Lowe},
  title     = {Fast Approximate Nearest Neighbors with Automatic Algorithm Configuration},
  booktitle = {International Conference on Computer Vision Theory and Application
               VISSAPP'09)},
  publisher = {INSTICC Press},
  year      = {2009},
  pages     = {331-340}
}
```
* Another C++ library for [http://www.cs.umd.edu/~mount/ANN/ ANN], slightly outdated.

# Time Series
## Delay Embedding
* [https://github.com/jwf/tdetools Geometric Template Matching] algorithm by [http://www.cs.mcgill.ca/~jfrank8/ Jordan Frank], packaged in a more friendly form at [https://github.com/pierrelux/tdetools]
* [http://www.mpipks-dresden.mpg.de/~tisean/Tisean_3.0.1/index.html Tisean] developed as part of the [http://www.amazon.com/Nonlinear-Series-Analysis-Holger-Kantz/dp/0521529026 Nonlinear Time Series Analysis] book. A feature-rich toolbox which can be used easily in conjunction with the UNIX tools through the pipe operator. Tisean however relies on Fortran and C code and tends to be extremely difficult to hack. It is better to use it as a blackbox.
