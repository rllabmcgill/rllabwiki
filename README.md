# Table of contents

* [Guide to new students](guide.md)
* [A list of great books](books.md)
* [Software resources](software.md)
* [Using the CLUMEQ clusters](clumeq.md)
* [RL and ML-oriented french glossary](french.md)

# How to edit

1. Join the [rllabmcgill](http://bitbucket.org/rllabmcgill)
2. Create a Markdown `.md` file in the [rllabwiki](http://bitbucket.org/rllabmcgill/rllabwiki) repository
3. Git commit and git push
