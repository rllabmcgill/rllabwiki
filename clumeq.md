# Creating an Account
The first step to using the CLUMEQ clusters is to create an account through [https://ccdb.computecanada.ca/me/faq#how_to_sign_up Compute Canada]. You need to specify the CCRI of your supervisor which will need to be obtained by asking them. Once your Compute Canada account is activated, you must create a CLUMEQ account by [https://ccdb.computecanada.ca/security/login logging into] your Compute Canada account then apply for a 'Consortium Account' which you should be able to find in the 'My Account' menu. You should select CLUMEQ as your Consortium.

= Using the CLUMEQ Clusters =
Two clusters are available to you through CLUMEQ, [http://www.hpc.mcgill.ca/ Guillimin] and Colosse. Both clusters use a version of [http://en.wikipedia.org/wiki/Moab_Cluster_Suite MOAB] for scheduling jobs so can share parts of scripts. Most of the information in this page comes from the [https://www.clumeq.ca/wiki/index.php/Main_Page CLUMEQ wiki].

You can ssh into the cluster at the following urls:
* Guillimin: guillimin.clumeq.ca
* Colosse: colosse.clumeq.ca

'''IMPORTANT''': The ssh server must not be used for any computation. They should strictly be used for monitoring and launching jobs.

== Using Guillimin ==
Guillimin is the most powerful of the two clusters. It is also the easiest to use. Some information about Guillimin and its usage can be found on the [http://www.hpc.mcgill.ca/index.php/starthere McGill HPC website].

=== Basic ===
To run code on Guillimin, you only need to submit a bash script with the code required to run. This script also must contain some required parameter for the MOAB scheduler. Each MOAB parameter is preceded by '#PBS'.

<code>
 #!/bin/bash
 ##################### START OF SCHEDULER ARGUMENTS #####################
 #PBS -l nodes=1:ppn=1            # resource requirements per task (i.e. 1 node, 1 core)
 #PBS -l walltime=2:00:00         # Upper bound on time to run task
 #PBS -o /mylogs/logs.txt         # path to file where stdin is dumped
 #PBS -e /myerrorlogs/error.txt   # path to file where stderr is dumped
 #PBS -A abc-123-aa               # Supervisor's project code
 #PBS -N mytaskname               # name of the task
 ##################### END OF SCHEDULER ARGUMENTS #####################

 ./runyourcode
</code>

You can then submit your script by using the following command on the ssh servers:
<code>
 msub -q sw ./yourscript
</code>

=== Array Tasks ===
Task can be organized in arrays. This feature allows for many tasks running the same code to be launched with only one script. Each sub-task generated is given an index in which can be used to access different data. Arrays of tasks are defined in a very similar way as simple tasks:

<code>
 #!/bin/bash
 ##################### START OF SCHEDULER ARGUMENTS #####################
 #PBS -l nodes=1:ppn=1            # resource requirements per sub-task (i.e. 1 node, 1 core)
 #PBS -l walltime=2:00:00         # Upper bound on time to run task
 #PBS -o /mylogs/logs.txt         # path to file where stdin is dumped
 #PBS -e /myerrorlogs/error.txt   # path to file where stderr is dumped
 #PBS -A abc-123-aa               # Supervisor's project code
 #PBS -N mytaskname               # name of the task array
 #PBS -t [0-99]                   # generate sub-tasks, indexed 0 to 99 (inclusive)
 ##################### END OF SCHEDULER ARGUMENTS #####################

 INDEX=  $MOAB_JOBARRAYINDEX      # the index of the sub-task can be accessed through some environment variables
 ./runyourcode ./data/$INDEX
</code>

The index increment can be modified. This is done by replacing "[0-99]" by "[0-99:INCR]" where INCR is some number. For example, if we wish to only have the even indices from 0 to 99, we need to write "[0-99:2]". Note that the environment variables are not available in the scheduler's argument so we cannot specify indices in the header.

You can then submit your script using the same command as previously except with the added "-V" tag, which gives access to the script to the environment variables:
<code>
 msub -V -q sw ./yourscript
</code>


=== Task Dependencies ===
Soon...

=== Script Generation examples ===
Soon... but not as soon as task dependencies...
