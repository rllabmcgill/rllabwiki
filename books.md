# Reinforcement Learning

* [Reinforcement Learning: An Introduction](http://www.amazon.ca/Reinforcement-Learning-Introduction-Richard-Sutton/dp/0262193981/)

```
 @book{SuttonBarto1998,
    author = {Sutton, Richard S. and Barto, Andrew G.},
    howpublished = {Hardcover},
    isbn = {0262193981},
    publisher = {MIT Press},
    address = {Cambridge, Massachusetts},
    title = {Reinforcement Learning: An Introduction (Adaptive Computation and Machine Learning)},
    year = {1998}
 }
 ```

* [Algorithms for Reinforcement Learning](http://www.amazon.ca/Reinforcement-Learning-Algorithms-Decision-Processes/dp/1608454924/)

```
 @book{Csaba2010,
  author    = {Csaba Szepesv{\'a}ri},
  title     = {Algorithms for Reinforcement Learning},
  booktitle = {Algorithms for Reinforcement Learning},
  publisher = {Morgan {\&} Claypool Publishers},
  series    = {Synthesis Lectures on Artificial Intelligence and Machine
               Learning},
  year      = {2010},
  ee        = {http://dx.doi.org/10.2200/S00268ED1V01Y201005AIM009}
}
```

* [Reinforcement Learning and Dynamic Programming Using Function Approximators](http://www.amazon.ca/Reinforcement-Learning-Programming-Function-Approximators/dp/1439821089/). 
* [Approximate Dynamic Programming: Solving the Curses of Dimensionality](http://www.amazon.ca/Approximate-Dynamic-Programming-Solving-Dimensionality/dp/047060445X/ )
* [Neuro-Dynamic Programming](http://www.amazon.ca/Neuro-Dynamic-Programming-Optimization-Neural-Computation/dp/1886529108 )
* [Dynamic Programming and Optimal Control](http://www.amazon.ca/gp/product/1886529086/)
* [Markov Decision Processes: Discrete Stochastic Dynamic Programming](http://www.amazon.ca/Markov-Decision-Processes-Stochastic-Programming/dp/0471727822/ )
* [Introduction to Stochastic Dynamic Programming](http://www.amazon.ca/Introduction-Stochastic-Dynamic-Programming-Sheldon/dp/0125984219/ )

```
 @book{Ross1983,
    author = {Ross, Sheldon},
    publisher = {New York - London etc.: Academic Press.},
    series = {Probability and Mathematical Statistics.},
    title = {Introduction to stochastic dynamic programming.},
    year = {1983}
 }
 ```

# Information Theory
* [Elements of Information Theory](http://www.amazon.ca/Elements-Information-Theory-Thomas-Cover/dp/0471241954/ )
* [Information Theory, Inference and Learning Algorithms](http://www.amazon.ca/Information-Theory-Inference-Learning-Algorithms/dp/0521642981/  )

# Computational Learning Theory
* [Probably Approximately Correct: Nature's Algorithms for Learning and Prospering in a Complex World](http://www.amazon.ca/Probably-Approximately-Correct-Algorithms-Prospering/dp/0465032710/ )
* [The Minimum Description Length Principle](http://www.amazon.ca/The-Minimum-Description-Length-Principle/dp/0262072815/ )
* [An Introduction to Computational Learning Theory](http://ieeexplore.ieee.org/xpl/bkabstractplus.jsp?bkn=6267405 )

# Optimization
* [Convex Optimization](http://www.amazon.ca/Convex-Optimization-Stephen-Boyd/dp/0521833787/ )

# Graph Theory
* [Spectral Graph Theory](http://www.amazon.ca/Spectral-Graph-Theory-Fan-Chung/dp/0821803158/ )

```
 @book{Chung1997,
    author = {Chung, Fan R. K.},
    publisher = {American Mathematical Society},
    title = {Spectral Graph Theory (CBMS Regional Conference Series in Mathematics, No. 92)},
    year = {1997}
}
```
